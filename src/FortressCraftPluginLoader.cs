﻿using System;
using uMod.Common;
using uMod.Plugins;

namespace uMod.Game.FortressCraft
{
    /// <summary>
    /// Responsible for loading the core plugin
    /// </summary>
    public class FortressCraftPluginLoader : PluginLoader
    {
        public override Type[] CorePlugins => new[] { typeof(FortressCraft) };

        public FortressCraftPluginLoader(ILogger logger) : base(logger)
        {
        }
    }
}
