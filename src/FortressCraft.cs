﻿using uMod.Plugins;
using uMod.Plugins.Decorators;

namespace uMod.Game.FortressCraft
{
    /// <summary>
    /// The core FortressCraft plugin
    /// </summary>
    [HookDecorator(typeof(ServerDecorator))]
    public class FortressCraft : Plugin
    {
        #region Initialization

        internal static readonly FortressCraftProvider Universal = FortressCraftProvider.Instance;

        /// <summary>
        /// Initializes a new instance of the FortressCraft class
        /// </summary>
        public FortressCraft()
        {
            // Set plugin info attributes
            Title = "FortressCraft";
            Author = FortressCraftExtension.AssemblyAuthors;
            Version = FortressCraftExtension.AssemblyVersion;
        }

        /// <summary>
        /// Called when the server is first initialized
        /// </summary>
        [Hook("OnServerInitialized")]
        private void OnServerInitialized()
        {
            // Add universal commands
            Universal.CommandSystem.DefaultCommands.Initialize(this);

            // Clean up invalid permission data
            permission.RegisterValidate(ExtensionMethods.IsSteamId);
            permission.CleanUp();
        }

        #endregion Initialization
    }
}
