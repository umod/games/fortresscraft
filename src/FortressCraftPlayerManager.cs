﻿using uMod.Auth;
using uMod.Common;

namespace uMod.Game.FortressCraft
{
    /// <summary>
    /// Represents a FortressCraft player manager
    /// </summary>
    public class FortressCraftPlayerManager : PlayerManager<FortressCraftPlayer>
    {
        /// <summary>
        /// Create a new instance of the FortressCraftPlayerManager class
        /// </summary>
        /// <param name="application"></param>
        /// <param name="logger"></param>
        public FortressCraftPlayerManager(IApplication application, ILogger logger) : base(application, logger)
        {
        }

        /// <summary>
        /// Determine if specified key matches the specified player
        /// </summary>
        /// <param name="partialNameOrIdOrIp"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        protected override bool IsPlayerKeyMatch(string partialNameOrIdOrIp, FortressCraftPlayer player)
        {
            if (base.IsPlayerKeyMatch(partialNameOrIdOrIp, player))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Determine if specified key matches the specified player
        /// </summary>
        /// <param name="partialNameOrIdOrIp"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        protected override bool IsPlayerKeyMatch(string partialNameOrIdOrIp, IPlayer player)
        {
            if (base.IsPlayerKeyMatch(partialNameOrIdOrIp, player))
            {
                return true;
            }

            return false;
        }
    }
}
